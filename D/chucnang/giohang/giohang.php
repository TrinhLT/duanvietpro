
<div class="product">
	<h2 class="h2-bar">Giỏ hàng của bạn</h2>
	
<?php 
	if(isset($_SESSION['giohang'])){

	// theem bớt số lượng san phâm
		 if(isset($_POST['sl'])){
			foreach ($_POST['sl'] as $id_sp=>$sl){
				if($sl==0){
					unset($_SESSION['giohang'][$id_sp]);
				}
				else if($sl>0){
					$_SESSION['giohang'][$id_sp]=$sl;
				}
			}
		 }


	// ------>

	 $arrId=array();
	//   foreach($_SESSION['giohang'] as $id_sp =>$value) {
	//     $arrId[] = $id_sp;
	// } // 2cach
	$arrId = array_keys($_SESSION['giohang']); // lay ra key 
	$strId = implode(',', $arrId);  // chuyeenr ve string 

	$sql ="SELECT * FROM sanpham WHERE id_sp IN($strId) ORDER BY id_sp DESC";
	$query = mysqli_query($conn,$sql);

?>
	<form id="giohang" method="post">
	<table  class="table">
		<thead>
			<tr>
				<th style="width:40%">Sản phẩm</th>
				<th style="width:10%">Giá</th>
				<th style="width:10%">Số lượng</th>
				<th style="width:30%" class="text-center">Tổng tiền</th>
				<th style="width:10%"></th>
			</tr>
		</thead>
		<?php
			$TotalPriceAll = 0;
			while ( $row = mysqli_fetch_array($query)) {
				$totalPrice = $row['gia_sp']*$_SESSION['giohang'][$row['id_sp']];
		?>
		<tbody>
			<tr>
				<td>
					<div class="row">
						<div class="col-sm-2 hidden-xs">
							<img src="quantri/anh/<?php echo $row['anh_sp'];?>" alt="..." class="img-responsive"/></div>
						<div class="col-sm-10">
							<h5><?php echo $row['ten_sp'];?></h5>
						</div>
					</div>
				</td>
				<td ><?php echo $row['gia_sp']; ?></td>
				<td>
					<input name="sl[<?php echo $row['id_sp']?>]"  type="number" min="1" class="form-control text-center" 
					value="<?php echo $_SESSION['giohang'][$row['id_sp']]; ?>">
				</td>
				<td data-th="Subtotal" class="text-center"><span><?php echo $totalPrice ;?></span></td>
				<td class="actions" data-th="">
					<button class="btn btn-danger btn-sm">
						 <a href="chucnang/giohang/xoahang.php?id_sp=<?php echo $row['id_sp']; ?>">Xóa</a>
					</button>
				</td>
			</tr>
		</tbody>
		<?php
			 $TotalPriceAll += $totalPrice;
			} 
		?>
		<tfoot>
			<tr>
				<td>
					<a href="index.php" class="btn btn-warning">Tiếp tục mua hàng</a>
					<a onclick="document.getElementById('giohang').submit();" href="#" class="btn btn-info">Cập nhật giỏ hàng</a>  
				</td>
				<td ><a  class="btn btn-primary
				" href="chucnang/giohang/xoahang.php?id_sp=<?php echo $row['id_sp']; ?>">Xoa ALL </a>
				</td>
				<td></td>
				<td class="hidden-xs text-center"><strong>Tổng tiền giỏ hàng: 
					<span><?php echo $TotalPriceAll;?></span></strong></td>
				<td><a href="index.php?template=muahang" class="btn btn-success btn-block">Thanh toán</a></td>
			</tr>
		</tfoot>
	</table>
</form>
</div>
<?php 
	}
	else{
		echo '<script>alert("ban mua hang di") </script>';
	}
?>
