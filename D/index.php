<?php 
include_once 'ketnoi.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title> Trang chủ</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css"  href='css/trangchu.css'>
	 <?php
        if (isset($_GET['template'])) {
            switch ($_GET['template']) {
                case 'danhsachtimkiem':
                    echo '<link rel="stylesheet" href="css/danhsachtimkiem.css">';
                    break;
                case 'chitietsp':
                    echo '<link rel="stylesheet" href="css/chitietsp.css">';
                    break;
                case 'giohang':
                    echo '<link rel="stylesheet" href="css/giohang.css">';
                    break;
                case 'muahang':
                    echo '<link rel="stylesheet" href="css/muahang.css">';
                    break;
                case 'hoanthanh':
                    echo '<link rel="stylesheet" href="css/hoanthanh.css">';
                    break;
            }
        }

        
        ?>
</head>
<body>
<div id="header">
	<div class="container" >
		<div class="row">
			<div id="logo" class="col-md-2 col-sm-12 col-xs-12 text-center">
				<h1><a href="#"><img src="images/logo.png"></a>					
				</h1>
			</div>
			
			<?php 
				include_once 'chucnang/timkiem/timkiem.php';
				include_once 'chucnang/giohang/giohangcuaban.php';
			?>
			
			<div id="dangky" class="col-md-2 col-sm-12 col-xs-12">
				<div>
					<li>
						<a href="index.php?template=dangky"><span class="glyphicon glyphicon-user"></span> Sign Up</a>
					</li>
					<li><a href="index.php?template=dangnhap"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="menu">
	<nav class="navbar navbar-inverse ">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="index.php">WebSiteName</a>
			</div>
			<ul class="nav navbar-nav">
				<li class="active"><a href="index.php?template=inde">Home</a></li>
				<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">SẢN PHẨM<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="index.php?template=danhsachsanpham">Page 1-1</a></li>
						<li><a href="index.php?template=danhsachsanpham">Page 1-2</a></li>
						<li><a href="index.php?template=danhsachsanpham">Page 1-3</a></li>
					</ul>
				</li>
				<li><a href="index.php?template=danhsachtintuc">TIN TỨC</a></li>
				<li><a href="#">LIÊN HỆ</a></li>
			</ul>
		</div>
	</nav>
</div>
<div class="body">
	<div class="container">
		<div class="row">
			<div id="main" class="col-md-9 col-sm-12 col-xs-12">
				<div id="slider">
					<div id="myCarousel" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
							<li data-target="#myCarousel" data-slide-to="1"></li>
							<li data-target="#myCarousel" data-slide-to="2"></li>
						</ol>
						<div class="carousel-inner">
							<div class="item active">
								<img src="images/slide-1.png" alt="Vietpro Academy">
							</div>					
							<div class="item">
								<img src="images/slide-2.png" alt="Vietpro Academy">
							</div>
							<div class="item">
								<img src="images/slide-3.png" alt="Vietpro Academy">
							</div>
						</div>
						<a class="left carousel-control" href="#myCarousel" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="right carousel-control" href="#myCarousel" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right"></span>
							<span class="sr-only">Next</span>
						</a>
					</div>
				</div>
			</div>
			<div id="sidebar" class="col-md-3 col-sm-12 col-xs-12">
				<div class="banner-l-item">
					<a href="#"><img class="img-responsive images1" src="images/banner-l-6.png" ></a>
				</div>
				<div class="banner-l-item">
					<a href="#"><img class="img-responsive" src="images/banner-l-5.png"></a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3 col-sm-12 col-xs-12">
				<?php  
					include_once 'chucnang/sanpham/danhmucsp.php'; 
					include_once 'chucnang/thongke/thongke.php'; 
				?>
				<div>
					
				</div>
			</div>
			<div class="col-md-9 col-sm-12 col-xs-12">
				<?php 
                    if (isset($_GET['template'])) {
                        switch ($_GET['template']) {
                            case 'danhsachtimkiem':
                                include_once 'chucnang/timkiem/danhsachtimkiem.php';
                                break;
                            case 'danhsachsp':
                                include_once 'chucnang/sanpham/danhsachsp.php';
                                break;
                            case 'chitietsp':
                                include_once 'chucnang/sanpham/chitietsp.php';
                                break;
                            case 'giohang':
                                include_once 'chucnang/giohang/giohang.php';
                                break;
                            case 'muahang':
                                include_once 'chucnang/giohang/muahang.php';
                                break;
                            case 'hoanthanh':
                                include_once 'chucnang/giohang/hoanthanh.php';
                                break;
                        }
                    } else {
                        include_once 'chucnang/sanpham/sanpham.php';
                    }
                    ?>
				
			</div>
		</div>
</div>

<div id="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-2 col-xs-12 col-sm-12">
				<div id="footer1">
					<h3> Thông tin </h3>
					<ul>
						<li><a href="#"> Trang chủ </a></li>
						<li><a href="#"> Giới thiệu </a></li>
						<li><a href="#"> Sản phẩm  </a>  </li>
						<li><a href="#"> Trang chủ </a></li>
						<li><a href="#"> Giới thiệu </a></li>
						
					</ul>
				</div>
			</div>
			<div class="col-md-2 col-xs-12 col-sm-12">
				<div id="footer1">
					<h3> chinh sach</h3>
					<ul>
						<li><a href="#"> Hướng dẫn mua hàng </a></li>
						<li><a href="#"> Chính sách mua hàng </a></li>
						<li><a href="#">  Hướng dẫn đổi trả </a></li>
						<li><a href="#"> Hệ thông của hàng </a></li>					
					</ul>
				</div>
			</div>
			<div class="col-md-4 col-xs-12 col-sm-12">
				<div id="footer1">
					<h3> LIên hệ</h3>
					<ul>
						<li>
							<span class="glyphicon glyphicon-home"></span>
							<span>Phòng 214, Chung cư B3, Phường Quan Hoa , Cầu Giấy, Hà Nội, Việt Nam</span>
						</li>
						<li> 
							<span class="glyphicon glyphicon-earphone"></span>
							<span>021489213652</span>
						</li>
						<li>
							<span class="glyphicon glyphicon-leaf"></span>
							<span>halymedia@gmail.com</span>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-md-4 col-xs-12 col-sm-12">
				<div id="footer1">
					<h3>đăng ký nhận tin  </h3>
					<p> Hãy gửi chúng tôi địa chỉ email của bạn, chúng tôi sẽ gửi cho bạn những ưu đãi đặc biêt.</p>
					<form>
						<input type="text" name="" value="Nhập email">
						<input type="button" name="" value="GỬI ĐI">
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="footer-b">
	<div class="container">
		<div class="row">
			<div class="footer-b-item col-md-6 col-sm-12 col-xs-12">
				<p>Học viện Công nghệ Vietpro - www.vietpro.edu.vn</p>
			</div>
			<div class="footer-b-item col-md-6 col-sm-12 col-xs-12">
				<p>© 2017 Vietpro Academy. All Rights Reserved</p>
			</div>
		</div>
	</div>
	
	<div id="scroll"><a href="#"><img src="images/scroll.png"></a></div>
</div>
</body>
</html>